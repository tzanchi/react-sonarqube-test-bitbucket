import React from 'react'
import { render } from '@testing-library/react' 
import Header from '../../src/components/Header'

describe('Header', () => {
  it('Renders without error', () => {
    const { queryByTestId } = render(
      <Header />
    )

    expect(queryByTestId('header-wrapper')).not.toBeNull()
  })
})