module.exports = (api) => {
  // We need to tweak the configuration during automated testing.
  const isTest = api.env('test')

  api.cache(!isTest)

  const presets = ['next/babel']

  return {
    presets,
  }
}
