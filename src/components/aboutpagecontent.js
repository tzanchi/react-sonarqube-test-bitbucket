import Link from "next/link"

const AboutPageContent =
    <div>
        <h1>About</h1>
        <Link href="/">
            <a>goto home</a>
        </Link>
    </div>

export default AboutPageContent