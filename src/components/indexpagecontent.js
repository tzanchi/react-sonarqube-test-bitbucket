import Link from "next/link"
import Blog from "./blog"

const IndexPageContent =
    <div>
        <h1>Next.js</h1>
        <Link href="/about">
            <a title="About page">goto about</a>
            <Blog />
        </Link>
    </div>

export default IndexPageContent
